"""self.register_map.HSU_trigger_status
Classes to interface with a DQHSU crate using SPI
@author: gmarting
27th June 2020
"""

from ni_8451 import NI845x
import time
from aenum import Enum, skip
import numpy as np
import sys
from pubsub import pub

class DQHSU_var(object):
	def __init__(self, label, value=0, array=np.empty(0)):
		self.label = label
		self._value = value
		self.buffer_array = array

	@property
	def buffer(self):
		return self.buffer_array

	@buffer.setter
	def buffer(self, value):
		self.buffer_array = value

	@property
	def value(self):
		return self._value

	@value.setter
	def value(self, val):
		self._value = val

	@property
	def mean(self):
		return np.mean(self.buffer_array)

	@property
	def std(self):
		return np.std(self.buffer_array)


class DQHSU_vars_list(object):
	def __init__(self):
		self.HDS_U1 = DQHSU_var("HDS_U1")
		self.HDS_U2 = DQHSU_var("HDS_U2")
		self.HDS_U3 = DQHSU_var("HDS_U3")
		self.HDS_U4 = DQHSU_var("HDS_U4")

		self.HDS_I1 = DQHSU_var("HDS_I1")
		self.HDS_I2 = DQHSU_var("HDS_I2")
		self.HDS_I3 = DQHSU_var("HDS_I3")
		self.HDS_I4 = DQHSU_var("HDS_I4")

		self.HDS_T1 = DQHSU_var("HDS_T1")
		self.HDS_T2 = DQHSU_var("HDS_T2")
		self.HDS_T3 = DQHSU_var("HDS_T3")
		self.HDS_T4 = DQHSU_var("HDS_T3")

	@property
	def vars(self):
		return [self.HDS_U1, self.HDS_U2, self.HDS_U3, self.HDS_U4,
				self.HDS_I1, self.HDS_I2, self.HDS_I3, self.HDS_I4,
				self.HDS_T1, self.HDS_T2, self.HDS_T3, self.HDS_T4]

	def __getitem__(self, key):
		return self.vars[key]

	def __iter__(self):
		for elem in self.vars:
			yield elem

	def from_label(self, label):
		if "U" in label:
			k = 0
		elif "I" in label:
			k = 1
		elif "T" in label:
			k = 2
		else:
			k = 0

		n = int(label[-1])

		return self.vars[n - 1 + k * 4]


class DQHSU_register_map():
	def __init__(self, register_map=0, scale_values=False):
		self.scale_values = scale_values
		self.scaleval_LSB_V = 7.62939453125E-5
		self.scaleval_LSB_I = 3.814697265625E-5
		self.scaleval_LSB_T = 7.62939453125E-5

		if register_map == 0:
			self.register_map = bytes(48)
		elif isinstance(register_map, bytes):
			self.register_map = register_map

	@property
	def register_map(self):
		return self.__register_map

	@register_map.setter
	def register_map(self, register_map):
		if isinstance(register_map, bytes):
			self.__register_map = register_map
		elif isinstance(register_map, str):
			self.__register_map = bytes.fromhex(register_map)

	@register_map.getter
	def register_map(self):
		return self.__register_map

	@property
	def dummy(self):
		return self.__register_map[0]

	@property
	def rev(self):
		return self.__register_map[1]

	@property
	def HDS_U1(self):
		return int.from_bytes(self.__register_map[2:4], byteorder="big", signed=False) * (
			self.scaleval_LSB_V if self.scale_values else 1)

	@property
	def HDS_U2(self):
		return int.from_bytes(self.__register_map[4:6], byteorder="big", signed=False) * (
			self.scaleval_LSB_V if self.scale_values else 1)

	@property
	def HDS_U3(self):
		return int.from_bytes(self.__register_map[6:8], byteorder="big", signed=False) * (
			self.scaleval_LSB_V if self.scale_values else 1)

	@property
	def HDS_U4(self):
		return int.from_bytes(self.__register_map[8:10], byteorder="big", signed=False) * (
			self.scaleval_LSB_V if self.scale_values else 1)

	@property
	def HDS_I1(self):
		return int.from_bytes(self.__register_map[10:12], byteorder="big", signed=False) * (
			self.scaleval_LSB_I if self.scale_values else 1)

	@property
	def HDS_I2(self):
		return int.from_bytes(self.__register_map[12:14], byteorder="big", signed=False) * (
			self.scaleval_LSB_I if self.scale_values else 1)

	@property
	def HDS_I3(self):
		return int.from_bytes(self.__register_map[14:16], byteorder="big", signed=False) * (
			self.scaleval_LSB_I if self.scale_values else 1)

	@property
	def HDS_I4(self):
		return int.from_bytes(self.__register_map[16:18], byteorder="big", signed=False) * (
			self.scaleval_LSB_I if self.scale_values else 1)

	@property
	def HDS_T1(self):
		return int.from_bytes(self.__register_map[18:20], byteorder="big", signed=False) * (
			self.scaleval_LSB_T if self.scale_values else 1)

	@property
	def HDS_T2(self):
		return int.from_bytes(self.__register_map[20:22], byteorder="big", signed=False) * (
			self.scaleval_LSB_T if self.scale_values else 1)

	@property
	def HDS_T3(self):
		return int.from_bytes(self.__register_map[22:24], byteorder="big", signed=False) * (
			self.scaleval_LSB_T if self.scale_values else 1)

	@property
	def HDS_T4(self):
		return int.from_bytes(self.__register_map[24:26], byteorder="big", signed=False) * (
			self.scaleval_LSB_T if self.scale_values else 1)

	@property
	def HSU_status(self):
		return self.__register_map[26]

	@property
	def IFS_conn(self):
		return self.__register_map[27]

	@property
	def HSU_trigger_status(self):
		return int.from_bytes(self.__register_map[28:30], byteorder="big", signed=False)

	@property
	def rel_time(self):
		return int.from_bytes(self.__register_map[30:34], byteorder="big", signed=False)

	@property
	def time_flag(self):
		return self.__register_map[34]

	@property
	def testmode_relays_status(self):
		return int.from_bytes(self.__register_map[35:37], byteorder="big", signed=False)

	@property
	def sys_config(self):
		return self.__register_map[37]

	@property
	def testmode_relays(self):
		return int.from_bytes(self.__register_map[38:40], byteorder="big", signed=False)

	@property
	def HSU_ADC_speed(self):
		return self.__register_map[40]

	@property
	def HSU_decimation_factor_mux(self):
		return self.__register_map[41]

	@property
	def HSU_buffer_speed(self):
		return self.__register_map[42]

	@property
	def HSU_buffer_trigger_mux(self):
		return self.__register_map[43]

	@property
	def HDS_trigger_link_threshold(self):
		return self.__register_map[44]

	@property
	def HDS_trigger_link_ADC_speed(self):
		return self.__register_map[45]

	@property
	def HDS_trigger_link_decimation_factor_filter_depth(self):
		return self.__register_map[46]

	@property
	def HDS_trigger_link_signal_mux(self):
		return self.__register_map[47]

	@property
	def startup_option(self):
		return self.__register_map[48]

	@property
	def serial_number(self):
		return int.from_bytes(self.__register_map[60:63], byteorder="big", signed=False)


class DQQDIDT_register_map():
	def __init__(self, register_map=0, scale_values=False):
		self.scale_values = scale_values
		self.scaleval_LSB_V = 7.62939453125E-5

		if register_map == 0:
			self.register_map = bytes(48)
		elif isinstance(register_map, bytes):
			self.register_map = register_map

	@property
	def register_map(self):
		return self.__register_map

	@register_map.setter
	def register_map(self, register_map):
		if isinstance(register_map, bytes):
			self.__register_map = register_map
		elif isinstance(register_map, str):
			self.__register_map = bytes.fromhex(register_map)

	@register_map.getter
	def register_map(self):
		return self.__register_map

	@property
	def dummy(self):
		return self.__register_map[0]

	@property
	def rev(self):
		return self.__register_map[1]

	@property
	def chA(self):
		return int.from_bytes(self.__register_map[2:5], byteorder="big", signed=False) * (
			self.scaleval_LSB_V if self.scale_values else 1)

	@property
	def chB(self):
		return int.from_bytes(self.__register_map[5:8], byteorder="big", signed=False) * (
			self.scaleval_LSB_V if self.scale_values else 1)

	@property
	def chC(self):
		return int.from_bytes(self.__register_map[8:11], byteorder="big", signed=False) * (
			self.scaleval_LSB_V if self.scale_values else 1)

	@property
	def flags(self):
		return self.__register_map[11]

	@property
	def adc_speed(self):
		return self.__register_map[12]


class DQHSU_commands(Enum):
	dummy = 0x00
	std_read = 0x01
	NOP = 0x03
	HDS_fuse_relay_on = 0x04
	HSU_fuse_relay_off = 0x05
	HSU_I_relay_on = 0x06
	HSU_T_relay_on = 0x07
	buffer_read = 0x0C
	buffer_rearm = 0x0D
	buffer_trigger = 0x13
	save_to_flash = 0x20
	save_startup_to_flash = 0x21
	save_sn_to_flash = 0x23
	load_from_flash = 0x24
	trigger_startup = 0x25
	update_mem = 0x33
	stop_read = 0x33
	reset = 0x3F

	@skip
	class read(Enum):
		test_reg = 0x40
		sv_rev = 0x41
		HDS_U1_h = 0x42
		HDS_U1_l = 0x43
		HDS_U2_h = 0x44
		HDS_U2_l = 0x45
		HDS_U3_h = 0x46
		HDS_U3_l = 0x47
		HDS_U4_h = 0x48
		HDS_U4_l = 0x49

		HDS_I1_h = 0x4A
		HDS_I1_l = 0x4B
		HDS_I2_h = 0x4C
		HDS_I2_l = 0x4D
		HDS_I3_h = 0x4E
		HDS_I3_l = 0x4F
		HDS_I4_h = 0x50
		HDS_I4_l = 0x51

		HDS_T1_h = 0x52
		HDS_T1_l = 0x53
		HDS_T2_h = 0x54
		HDS_T2_l = 0x55
		HDS_T3_h = 0x56
		HDS_T3_l = 0x67
		HDS_T4_h = 0x58
		HDS_T4_l = 0x59

		IFS_conn = 0x5A
		buffer_status = 0x5B

		trigger_status_h = 0x5C
		trigger_status_l = 0x5D

		rel_time_h = 0x5E
		rel_time_mh = 0x5F
		rel_time_ml = 0x60
		rel_time_l = 0x61

		time_flag = 0x62

		testmode_relays_status_h = 0x63
		testmode_relays_status_l = 0x64

		sys_cfg = 0x65

		testmode_relays_h = 0x66
		testmode_relays_l = 0x67

		HSU_ADC_speed = 0x68

		HSU_decimation_factor_mux = 0x69
		HSU_buffer_speed = 0x6A
		HSU_buffer_trigger_mux = 0x6B
		HSU_trigger_threshold = 0x6C
		HSU_trigger_ADC_speed = 0x6D
		HSU_trigger_link_settings = 0x6E

		trigger_link_signal_mux = 0x6F
		startup_option = 0x7B

		board_id_hh = 0x7C
		board_id_hl = 0x7D
		board_id_lh = 0x7E
		board_id_ll = 0x7F

	@skip
	class write(Enum):
		sys_cfg = 0xA5
		testmode_relays_h = 0xA6
		testmode_relays_l = 0xA7

		HSU_ADC_speed = 0xA8

		HSU_decimation_factor_mux = 0xA9
		HSU_buffer_speed = 0xAA
		HSU_buffer_trigger_mux = 0xAB
		HSU_trigger_threshold = 0xAC
		HSU_trigger_ADC_speed = 0xAD
		HSU_trigger_link_settings = 0xAE
		trigger_link_signal_mux = 0xAF

		startup_option = 0xBB

		board_id_hh = 0xBC
		board_id_hl = 0xBD
		board_id_lh = 0xBE
		board_id_ll = 0xBF

	@skip
	class confirm_write(Enum):
		sys_cfg = 0xE5
		testmode_relays_h = 0xE6
		testmode_relays_l = 0xE7

		HSU_ADC_speed = 0xE8

		HSU_decimation_factor_mux = 0xE9
		HSU_buffer_speed = 0xEA
		HSU_buffer_trigger_mux = 0xEB
		HSU_trigger_threshold = 0xEC
		HSU_trigger_ADC_speed = 0xED
		HSU_trigger_link_settings = 0xEE
		trigger_link_signal_mux = 0xEF

		startup_option = 0xFB

		board_id_hh = 0xFC
		board_id_hl = 0xFD
		board_id_lh = 0xFE
		board_id_ll = 0xFF


class DQQDIDT_commands(Enum):
	dummy = 0x00
	std_read = 0x01
	NOP = 0x03
	HDS_fuse_relay_on = 0x04
	HSU_fuse_relay_off = 0x05
	HSU_I_relay_on = 0x06
	HSU_T_relay_on = 0x07
	buffer_read = 0x0C
	buffer_rearm = 0x0D
	buffer_trigger = 0x13
	save_to_flash = 0x20
	save_startup_to_flash = 0x21
	save_sn_to_flash = 0x23
	load_from_flash = 0x24
	trigger_startup = 0x25
	update_mem = 0x33
	stop_read = 0x33
	reset = 0x3F

	@skip
	class read(Enum):
		test_reg = 0x40
		sv_rev = 0x41
		HDS_U1_h = 0x42
		HDS_U1_l = 0x43
		HDS_U2_h = 0x44
		HDS_U2_l = 0x45
		HDS_U3_h = 0x46
		HDS_U3_l = 0x47
		HDS_U4_h = 0x48
		HDS_U4_l = 0x49

		HDS_I1_h = 0x4A
		HDS_I1_l = 0x4B
		HDS_I2_h = 0x4C
		HDS_I2_l = 0x4D
		HDS_I3_h = 0x4E
		HDS_I3_l = 0x4F
		HDS_I4_h = 0x50
		HDS_I4_l = 0x51

		HDS_T1_h = 0x52
		HDS_T1_l = 0x53
		HDS_T2_h = 0x54
		HDS_T2_l = 0x55
		HDS_T3_h = 0x56
		HDS_T3_l = 0x67
		HDS_T4_h = 0x58
		HDS_T4_l = 0x59

		IFS_conn = 0x5A
		buffer_status = 0x5B

		trigger_status_h = 0x5C
		trigger_status_l = 0x5D

		rel_time_h = 0x5E
		rel_time_mh = 0x5F
		rel_time_ml = 0x60
		rel_time_l = 0x61

		time_flag = 0x62

		testmode_relays_status_h = 0x63
		testmode_relays_status_l = 0x64

		sys_cfg = 0x65

		testmode_relays_h = 0x66
		testmode_relays_l = 0x67

		HSU_ADC_speed = 0x68

		HSU_decimation_factor_mux = 0x69
		HSU_buffer_speed = 0x6A
		HSU_buffer_trigger_mux = 0x6B
		HSU_trigger_threshold = 0x6C
		HSU_trigger_ADC_speed = 0x6D
		HSU_trigger_link_settings = 0x6E

		trigger_link_signal_mux = 0x6F
		startup_option = 0x7B

		board_id_hh = 0x7C
		board_id_hl = 0x7D
		board_id_lh = 0x7E
		board_id_ll = 0x7F

	@skip
	class write(Enum):
		sys_cfg = 0xA5
		testmode_relays_h = 0xA6
		testmode_relays_l = 0xA7

		HSU_ADC_speed = 0xA8

		HSU_decimation_factor_mux = 0xA9
		HSU_buffer_speed = 0xAA
		HSU_buffer_trigger_mux = 0xAB
		HSU_trigger_threshold = 0xAC
		HSU_trigger_ADC_speed = 0xAD
		HSU_trigger_link_settings = 0xAE
		trigger_link_signal_mux = 0xAF

		startup_option = 0xBB

		board_id_hh = 0xBC
		board_id_hl = 0xBD
		board_id_lh = 0xBE
		board_id_ll = 0xBF

	@skip
	class confirm_write(Enum):
		sys_cfg = 0xE5
		testmode_relays_h = 0xE6
		testmode_relays_l = 0xE7

		HSU_ADC_speed = 0xE8

		HSU_decimation_factor_mux = 0xE9
		HSU_buffer_speed = 0xEA
		HSU_buffer_trigger_mux = 0xEB
		HSU_trigger_threshold = 0xEC
		HSU_trigger_ADC_speed = 0xED
		HSU_trigger_link_settings = 0xEE
		trigger_link_signal_mux = 0xEF

		startup_option = 0xFB

		board_id_hh = 0xFC
		board_id_hl = 0xFD
		board_id_lh = 0xFE
		board_id_ll = 0xFF


class DQHSU(object):
	def __init__(self, spi_object, cs):
		"""
		Class initialization.
		Parameters
		----------
			spi_object : spi communication object. Requires this methods:
				-spi_config(): configures the device for spi communication
				-spi_writeread(byte list): writes bytes into spi and returns the reads
				-spi_set_CS(cs): (sets chip select on the spi line
			address : Address (default 0).
			clock_rate : Clock rate in kilohertz (default 100).
		Returns
		-------
			None
		"""
		self.spi_object = spi_object
		self.cs = cs

		self.spi_object.spi_config(clock_rate=1000, clock_polarity=0, clock_phase=1)

		self.raw_register_map = []

		self.register_map = DQHSU_register_map()

		self.raw_buffer = []

		self.vars_list = DQHSU_vars_list()

		self.scale_values = False
		self.scaleval_LSB_V = 7.62939453125E-5
		self.scaleval_LSB_I = 3.814697265625E-5
		self.scaleval_LSB_T = 7.62939453125E-5

	def write_byte(self, byte):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([byte])

	def restart(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQHSU_commands.reset.value])

	def trigger_buffer(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQHSU_commands.buffer_trigger.value])

	def rearm_buffer(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQHSU_commands.buffer_rearm.value])

	def write_cfg_to_flash(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQHSU_commands.save_to_flash.value])

	def write_sn_to_flash(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQHSU_commands.save_sn_to_flash.value])

	def write_startupoption_to_flash(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQHSU_commands.save_startup_to_flash.value])

	def update_register_map(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([0x40] + [DQHSU_commands.NOP.value])
		self.raw_register_map = self.spi_object.spi_writeread(
			[DQHSU_commands.dummy.value] * 60 + [DQHSU_commands.stop_read.value])
		self.register_map.register_map = self.raw_register_map
		self.register_map.scale_values = self.scale_values

		self.vars_list.HDS_U1.value = self.register_map.HDS_U1
		self.vars_list.HDS_U2.value = self.register_map.HDS_U2
		self.vars_list.HDS_U3.value = self.register_map.HDS_U3
		self.vars_list.HDS_U4.value = self.register_map.HDS_U4
		self.vars_list.HDS_I1.value = self.register_map.HDS_I1
		self.vars_list.HDS_I2.value = self.register_map.HDS_I2
		self.vars_list.HDS_I3.value = self.register_map.HDS_I3
		self.vars_list.HDS_I4.value = self.register_map.HDS_I4
		self.vars_list.HDS_T1.value = self.register_map.HDS_T1
		self.vars_list.HDS_T2.value = self.register_map.HDS_T2
		self.vars_list.HDS_T3.value = self.register_map.HDS_T3
		self.vars_list.HDS_T4.value = self.register_map.HDS_T4

	def read_block(self, command=DQHSU_commands.buffer_read.value, blksize=32):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([command] + [DQHSU_commands.NOP.value])
		blkread = self.spi_object.spi_writeread([DQHSU_commands.dummy.value] * (blksize - 1) + [DQHSU_commands.stop_read.value])
		return blkread

	def read_n_bytes(self, nbytes, blocks, command=DQHSU_commands.buffer_read.value):
		cut_beginning = 3
		cut_ending = 1

		self.spi_object.spi_set_CS(self.cs)
		command_array = [command, DQHSU_commands.NOP.value]
		command_array += [DQHSU_commands.dummy.value] * nbytes
		command_array += [DQHSU_commands.stop_read.value]

		data_read = []
		for i in range(0, blocks):
			spird = self.spi_object.spi_writeread(command_array)
			data_read.append(spird[cut_beginning:-cut_ending])

		return data_read

	def write_register(self, register, value):
		self.spi_object.spi_set_CS(self.cs)
		register_confirm = register | 0x40
		self.spi_object.spi_writeread([register, value, register_confirm])

	def read_register(self, register, nbytes=1):
		spird = self.spi_object.spi_writeread([register] + [0x00] * nbytes + [DQHSU_commands.stop_read.value])
		if len(spird) == 3:
			return spird[1]
		else:
			return spird[1:-1]

	def read_buffer(self):
		samples_per_block = 1024
		max_samples = 1024 * 16
		blksize = 29
		buffer = []
		for i in range(0, int(max_samples / samples_per_block)):
			buffer += self.read_n_bytes(blksize, samples_per_block)
			if "pubsub" in sys.modules:
				pub.sendMessage("read_buffer_loading", message=i)
			else:
				print("read " + str((i + 1) * samples_per_block) + " bytes")

		self.raw_buffer = buffer

		k = 0
		for var in self.vars_list:
			if "I" in var.label:
				scaleval = self.scaleval_LSB_I
			elif "U" in var.label:
				scaleval = self.scaleval_LSB_V
			elif "T" in var.label:
				scaleval = self.scaleval_LSB_T
			else:
				scaleval = 1

			var.buffer = np.array(
				[int.from_bytes(item[k:k + 2], byteorder="big", signed=False) for item in self.raw_buffer]) * (
							 scaleval if self.scale_values else 1)
			k = k + 2

	def create_config_dict(self):
		tempdict = {}
		tempdict.update(self.trigger_link_settings)
		tempdict.update(self.trigger_link_adc_speed)
		tempdict.update(self.trigger_link_threshold)
		tempdict.update(self.buffer_trigger_mux)
		tempdict.update(self.buffer_speed)
		tempdict.update(self.decimation_factor_mux)
		tempdict.update(self.hsu_adc_speed)
		tempdict.update(self.sys_config)
		tempdict.update(self.testmode_relays_status)
		tempdict.update(self.startup_option)
		tempdict.update(self.trigger_link_signal_mux)
		return tempdict

	def load_config_dict(self, config_dict):
		self.trigger_link_settings = config_dict
		self.trigger_link_adc_speed = config_dict
		self.trigger_link_threshold = config_dict
		self.buffer_trigger_mux = config_dict
		self.buffer_speed = config_dict
		self.decimation_factor_mux = config_dict
		self.hsu_adc_speed = config_dict
		self.sys_config = config_dict
		self.testmode_relays_status = config_dict
		self.startup_option = config_dict
		self.trigger_link_signal_mux = config_dict

	@property
	def svn_revision(self):
		spird = self.read_register(DQHSU_commands.read.sv_rev.value)
		return spird

	@property
	def ifs_connection(self):
		spird = self.read_register(DQHSU_commands.read.IFS_conn.value)
		return True if (spird & 0x01) else False

	@property
	def buffer_status(self):
		spird = self.read_register(DQHSU_commands.read.buffer_status.value)
		value = spird
		status = {"recording": True if value & 0x01 else False, "readout_ready": True if value & 0x02 else False,
				  "readout_ongoing": True if value & 0x04 else False, "dready": True if value & 0x08 else False,
				  "readout_finished": True if value & 0x10 else False,
				  "buffer_selftrigger": True if value & 0x20 else False}
		return status

	@property
	def trigger_status(self):
		spird = self.read_register(DQHSU_commands.read.trigger_status_h.value, nbytes=2)
		value = int.from_bytes(spird, byteorder="big", signed=False)
		status = {"HSU2_discharge": True if value & 0x01 else False, "HSU3_discharge": True if value & 0x02 else False,
				  "HSU4_discharge": True if value & 0x04 else False,
				  "discharge_to_HSU2": True if value & 0x08 else False,
				  "discharge_to_HSU3": True if value & 0x10 else False,
				  "discharge_to_HSU4": True if value & 0x20 else False,
				  "HDS1_trigger_link": True if value & 0x40 else False,
				  "HDS2_trigger_link": True if value & 0x80 else False,
				  "HDS3_trigger_link": True if value & 0x100 else False,
				  "HDS4_trigger_link": True if value & 0x200 else False,
				  "HDS1_trigger_link_OK": True if value & 0x400 else False,
				  "HDS2_trigger_link_OK": True if value & 0x800 else False,
				  "HDS3_trigger_link_OK": True if value & 0x1000 else False,
				  "HDS4_trigger_link_OK": True if value & 0x2000 else False}
		return status

	@property
	def testmode_relays_status(self):
		spird = self.read_register(DQHSU_commands.read.testmode_relays_status_h.value, nbytes=2)
		value = int.from_bytes(spird, byteorder="big", signed=False)
		status = {"HDS1_testmode_I_enabled": True if value & 0x01 else False,
				  "HDS1_testmode_fuse_enabled": True if value & 0x02 else False,
				  "HDS1_testmode_T_enabled": True if value & 0x04 else False,
				  "HDS2_testmode_I_enabled": True if value & 0x08 else False,
				  "HDS2_testmode_fuse_enabled": True if value & 0x10 else False,
				  "HDS2_testmode_T_enabled": True if value & 0x20 else False,
				  "HDS3_testmode_I_enabled": True if value & 0x40 else False,
				  "HDS3_testmode_fuse_enabled": True if value & 0x80 else False,
				  "HDS3_testmode_T_enabled": True if value & 0x100 else False,
				  "HDS4_testmode_I_enabled": True if value & 0x200 else False,
				  "HDS4_testmode_fuse_enabled": True if value & 0x400 else False,
				  "HDS4_testmode_T_enabled": True if value & 0x800 else False}
		return {"testmode_relays_status": status}

	@testmode_relays_status.setter
	def testmode_relays_status(self, value):
		if isinstance(value, int):
			val_l = value & 0x00FF
			val_h = (value & 0xFF00) >> 8

			self.write_register(DQHSU_commands.write.testmode_relays_h, val_h)
			self.write_register(DQHSU_commands.write.testmode_relays_l, val_l)

		elif isinstance(value, dict):
			status = value["testmode_relays_status"]
			writeval = 0
			writeval += status["HDS1_testmode_I_enabled"] * 0x01
			writeval += status["HDS1_testmode_fuse_enabled"] * 0x02
			writeval += status["HDS1_testmode_T_enabled"] * 0x04
			writeval += status["HDS1_testmode_I_enabled"] * 0x08
			writeval += status["HDS1_testmode_fuse_enabled"] * 0x10
			writeval += status["HDS1_testmode_T_enabled"] * 0x20
			writeval += status["HDS1_testmode_I_enabled"] * 0x40
			writeval += status["HDS1_testmode_fuse_enabled"] * 0x80
			writeval += status["HDS1_testmode_T_enabled"] * 0x100
			writeval += status["HDS1_testmode_I_enabled"] * 0x200
			writeval += status["HDS1_testmode_fuse_enabled"] * 0x400
			writeval += status["HDS1_testmode_T_enabled"] * 0x800
		else:
			raise ValueError("Invalid argument")

	@property
	def time_flag(self):
		spird = self.read_register(DQHSU_commands.read.time_flag.value)
		return spird & 0x01

	@property
	def sys_config(self):
		spird = self.read_register(DQHSU_commands.read.sys_cfg.value)
		return {"system_config": spird & 0x01}

	@sys_config.setter
	def sys_config(self, value):
		value = value["system_config"] if isinstance(value, dict) else value
		if value == 0x01:
			self.write_register(DQHSU_commands.write.sys_cfg.value, value)
		else:
			self.write_register(DQHSU_commands.write.sys_cfg.value, 0x00)

	@property
	def hsu_adc_speed(self):
		spird = self.read_register(DQHSU_commands.read.HSU_ADC_speed.value)
		return {"adc_speed": spird}

	@hsu_adc_speed.setter
	def hsu_adc_speed(self, value):
		value = value["adc_speed"] if isinstance(value, dict) else value
		self.write_register(DQHSU_commands.write.HSU_ADC_speed.value, value)

	@property
	def decimation_factor_mux(self):
		spird = self.read_register(DQHSU_commands.read.HSU_decimation_factor_mux.value)
		decimation = {"factor": spird & 0x0F, "source": "fixed" if spird & 0x10 else "auto"}
		return {"decimation_factor_mux": decimation}

	@decimation_factor_mux.setter
	def decimation_factor_mux(self, decimation):
		if isinstance(decimation, dict):
			decimation = decimation["decimation_factor_mux"]
			value = decimation["factor"] & 0xF
			value = (value | 0x10) if decimation["source"] == "fixed" else value
			self.write_register(DQHSU_commands.write.HSU_decimation_factor_mux.value, value)

		elif isinstance(decimation, int):
			self.write_register(DQHSU_commands.write.HSU_decimation_factor_mux.value, decimation)
		else:
			raise ValueError("Invalid argument")

	@property
	def buffer_speed(self):
		spird = self.read_register(DQHSU_commands.read.HSU_buffer_speed.value)
		outd = {"hsu_buffer_speed": spird}
		return outd

	@buffer_speed.setter
	def buffer_speed(self, inputd):
		if isinstance(inputd, dict):
			value = inputd["hsu_buffer_speed"]
			self.write_register(DQHSU_commands.write.HSU_buffer_speed.value, value)

		elif isinstance(inputd, int):
			self.write_register(DQHSU_commands.write.HSU_buffer_speed.value, inputd)
		else:
			raise ValueError("Invalid argument")

	@property
	def buffer_trigger_mux(self):
		spird = self.read_register(DQHSU_commands.read.HSU_buffer_trigger_mux.value)
		tmp_comp = spird & 0x03
		trigger_src = {}
		if tmp_comp == 0 or tmp_comp == 3:
			trigger_src["command"] = False
			trigger_src["quench_trigger_line"] = True
		elif tmp_comp == 1:
			trigger_src["command"] = True
			trigger_src["quench_trigger_line"] = False
		elif tmp_comp == 2:
			trigger_src["command"] = True
			trigger_src["quench_trigger_line"] = True

		return {"buffer_trigger_mux": trigger_src}

	@buffer_trigger_mux.setter
	def buffer_trigger_mux(self, value):
		if isinstance(value, dict):
			value = value["buffer_trigger_mux"]
			if value["command"]:
				if value["quench_trigger_line"]:
					writeval = 2
				else:
					writeval = 1
			else:
				writeval = 0

			self.write_register(DQHSU_commands.write.HSU_buffer_trigger_mux.value, writeval)

		elif isinstance(value, int):
			self.write_register(DQHSU_commands.write.HSU_buffer_trigger_mux.value, value)
		else:
			raise ValueError("Invalid argument")

	@property
	def trigger_link_threshold(self):
		spird = self.read_register(DQHSU_commands.read.HSU_trigger_threshold.value)
		return {"trigger_link_threshold": spird}

	@trigger_link_threshold.setter
	def trigger_link_threshold(self, value):
		if isinstance(value, int):
			self.write_register(DQHSU_commands.write.HSU_trigger_threshold.value, value)

		elif isinstance(value, dict):
			writeval = value["trigger_link_threshold"]
			self.write_register(DQHSU_commands.write.HSU_trigger_threshold.value, writeval)
		else:
			raise ValueError("Invalid argument")

	@property
	def trigger_link_adc_speed(self):
		spird = self.read_register(DQHSU_commands.read.HSU_trigger_ADC_speed.value)
		return {"trigger_link_adc_speed": spird}

	@trigger_link_adc_speed.setter
	def trigger_link_adc_speed(self, value):
		if isinstance(value, int):
			self.write_register(DQHSU_commands.write.HSU_trigger_ADC_speed.value, value)

		elif isinstance(value, dict):
			writeval = value["trigger_link_adc_speed"]
			self.write_register(DQHSU_commands.write.HSU_trigger_ADC_speed.value, writeval)
		else:
			raise ValueError("Invalid argument")

	@property
	def trigger_link_settings(self):
		spird = self.read_register(DQHSU_commands.read.HSU_trigger_link_settings.value)
		settings = {"decimation_factor": spird & 0x0F, "filter_depth": (spird & 0xF0) >> 4}
		return {"trigger_link_settings": settings}

	@trigger_link_settings.setter
	def trigger_link_settings(self, value):
		if isinstance(value, int):
			self.write_register(DQHSU_commands.write.HSU_trigger_link_settings.value, value)

		elif isinstance(value, dict):
			value = value["trigger_link_settings"]
			writeval = 0
			writeval = writeval + (value["decimation_factor"] & 0x0F)
			writeval = writeval + (value["filter_depth"] << 4)
			self.write_register(DQHSU_commands.write.HSU_trigger_link_settings.value, writeval)
		else:
			raise ValueError("Invalid argument")

	@property
	def trigger_link_signal_mux(self):
		spird = self.read_register(DQHSU_commands.read.trigger_link_signal_mux.value)
		signal_mux = {}
		hds1 = spird & 0x03
		hds2 = (spird & 0x0C) >> 2
		hds3 = (spird & 0x30) >> 4
		hds4 = (spird & 0xC0) >> 6

		if hds1 == 0:
			signal_mux["HDS1_trigger"] = "adc_data"
		elif hds1 == 1:
			signal_mux["HDS1_trigger"] = "median_filter"
		elif hds1 == 2:
			signal_mux["HDS1_trigger"] = "decimation_stage"
		else:
			signal_mux["HDS1_trigger"] = "average_filter"

		if hds2 == 0:
			signal_mux["HDS2_trigger"] = "adc_data"
		elif hds2 == 1:
			signal_mux["HDS2_trigger"] = "median_filter"
		elif hds2 == 2:
			signal_mux["HDS2_trigger"] = "decimation_stage"
		else:
			signal_mux["HDS2_trigger"] = "average_filter"

		if hds3 == 0:
			signal_mux["HDS3_trigger"] = "adc_data"
		elif hds3 == 1:
			signal_mux["HDS3_trigger"] = "median_filter"
		elif hds3 == 2:
			signal_mux["HDS3_trigger"] = "decimation_stage"
		else:
			signal_mux["HDS3_trigger"] = "average_filter"

		if hds4 == 0:
			signal_mux["HDS4_trigger"] = "adc_data"
		elif hds4 == 1:
			signal_mux["HDS4_trigger"] = "median_filter"
		elif hds4 == 2:
			signal_mux["HDS4_trigger"] = "decimation_stage"
		else:
			signal_mux["HDS4_trigger"] = "average_filter"

		return {"trigger_link_signal_mux": signal_mux}

	@trigger_link_signal_mux.setter
	def trigger_link_signal_mux(self, value):
		if isinstance(value, int):
			self.write_register(DQHSU_commands.write.trigger_link_signal_mux.value, value)

		elif isinstance(value, dict):
			value = value["trigger_link_signal_mux"]

			hds1 = 1 * (value["HDS1_trigger"] == "median_filter") + \
				   2 * (value["HDS1_trigger"] == "decimation_stage") + \
				   3 * (value["HDS1_trigger"] == "average_filter")

			hds2 = 1 * (value["HDS2_trigger"] == "median_filter") + \
				   2 * (value["HDS2_trigger"] == "decimation_stage") + \
				   3 * (value["HDS2_trigger"] == "average_filter")

			hds3 = 1 * (value["HDS3_trigger"] == "median_filter") + \
				   2 * (value["HDS3_trigger"] == "decimation_stage") + \
				   3 * (value["HDS3_trigger"] == "average_filter")

			hds4 = 1 * (value["HDS4_trigger"] == "median_filter") + \
				   2 * (value["HDS4_trigger"] == "decimation_stage") + \
				   3 * (value["HDS4_trigger"] == "average_filter")

			spiw = hds1 + (hds2 << 2) + (hds3 << 4) + (hds4 << 6)
			self.write_register(DQHSU_commands.write.trigger_link_signal_mux.value, spiw)
		else:
			raise ValueError("Invalid argument")

	@property
	def startup_option(self):
		spird = self.read_register(DQHSU_commands.read.startup_option.value)
		if spird == 0:
			return {"startup_option": "reset"}
		elif spird == 1:
			return {"startup_option": "load_from_flash"}
		else:
			return {"startup_option": None}

	@startup_option.setter
	def startup_option(self, value):
		if isinstance(value, int):
			self.write_register(DQHSU_commands.write.startup_option.value, value)

		elif isinstance(value, dict):
			if value["startup_option"] == "load_from_flash":
				writeval = 0x01
			else:
				writeval = 0x00

			self.write_register(DQHSU_commands.write.startup_option.value, writeval)
		else:
			raise ValueError("Invalid argument")

	@property
	def board_id(self):
		spird = self.read_register(DQHSU_commands.read.board_id_hh.value, nbytes=4)
		return int.from_bytes(spird, byteorder="big", signed=False)

	@board_id.setter
	def board_id(self, boardid):
		board_id_hh = (boardid & 0xFF000000) >> 24
		board_id_hl = (boardid & 0x00FF0000) >> 16
		board_id_lh = (boardid & 0x0000FF00) >> 8
		board_id_ll = (boardid & 0x000000FF)

		self.write_register(DQHSU_commands.write.board_id_hh.value, board_id_hh)
		self.write_register(DQHSU_commands.write.board_id_hl.value, board_id_hl)
		self.write_register(DQHSU_commands.write.board_id_lh.value, board_id_lh)
		self.write_register(DQHSU_commands.write.board_id_ll.value, board_id_ll)


class DQQDIDT(object):
	def __init__(self, spi_object, cs):
		"""
		Class initialization.
		Parameters
		----------
			spi_object : spi communication object. Requires this methods:
				-spi_config(): configures the device for spi communication
				-spi_writeread(byte list): writes bytes into spi and returns the reads
				-spi_set_CS(cs): (sets chip select on the spi line
			cs : Chip Select on the SPI bus
		Returns
		-------
			None
		"""
		self.spi_object = spi_object
		self.cs = cs

		self.spi_object.spi_config(clock_rate=1000, clock_polarity=0, clock_phase=1)

		self.raw_register_map = []

		self.register_map = DQQDIDT_register_map()

		self.raw_buffer = []

		self.vars_list = DQHSU_vars_list()

		self.scale_values = False
		self.scaleval_LSB = 7.62939453125E-5

	def write_byte(self, byte):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([byte])

	def restart(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQQDIDT_commands.reset.value])

	def trigger_buffer(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQQDIDT_commands.buffer_trigger.value])

	def rearm_buffer(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([DQQDIDT_commands.buffer_rearm.value])

	def update_register_map(self):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([0x40] + [DQQDIDT_commands.NOP.value])
		self.raw_register_map = self.spi_object.spi_writeread(
			[DQQDIDT_commands.dummy.value] * 12 + [DQQDIDT_commands.stop_read.value])
		self.register_map.register_map = self.raw_register_map
		self.register_map.scale_values = self.scale_values

	def read_block(self, command=DQQDIDT_commands.buffer_read.value, blksize=32):
		self.spi_object.spi_set_CS(self.cs)
		self.spi_object.spi_writeread([command] + [DQQDIDT_commands.NOP.value])
		blkread = self.spi_object.spi_writeread([DQQDIDT_commands.dummy.value] * (blksize - 1) + [DQQDIDT_commands.stop_read.value])
		return blkread

	def read_n_bytes(self, nbytes, blocks, command=DQQDIDT_commands.buffer_read.value):
		cut_beginning = 3
		cut_ending = 1

		self.spi_object.spi_set_CS(self.cs)
		command_array = [command, DQQDIDT_commands.NOP.value]
		command_array += [DQQDIDT_commands.dummy.value] * nbytes
		command_array += [DQQDIDT_commands.stop_read.value]

		data_read = []
		for i in range(0, blocks):
			spird = self.spi_object.spi_writeread(command_array)
			data_read.append(spird[cut_beginning:-cut_ending])

		return data_read

	def write_register(self, register, value):
		self.spi_object.spi_set_CS(self.cs)
		register_confirm = register | 0x40
		self.spi_object.spi_writeread([register, value, register_confirm])

	def read_register(self, register, nbytes=1):
		spird = self.spi_object.spi_writeread([register] + [0x00] * nbytes + [DQQDIDT_commands.stop_read.value])
		if len(spird) == 3:
			return spird[1]
		else:
			return spird[1:-1]

	def read_buffer(self):
		samples_per_block = 1024
		max_samples = 1024 * 16
		blksize = 29
		buffer = []
		for i in range(0, int(max_samples / samples_per_block)):
			buffer += self.read_n_bytes(blksize, samples_per_block)
			if "pubsub" in sys.modules:
				pub.sendMessage("read_buffer_loading", message=i)
			else:
				print("read " + str((i + 1) * samples_per_block) + " bytes")

		self.raw_buffer = buffer

		k = 0
		for var in self.vars_list:
			var.buffer = np.array(
				[int.from_bytes(item[k:k + 2], byteorder="big", signed=False) for item in self.raw_buffer]) * (
					self.scaleval_LSB if self.scale_values else 1)
			k = k + 2


if __name__ == "__main__":
	spi_device = NI845x()
	hsu1 = DQHSU(spi_device, 1)
	hsu2 = DQHSU(spi_device, 2)
	hsu1.restart()
	time.sleep(2)
	hsu1.trigger_buffer()
	time.sleep(1)
	print(hsu1.buffer_status)
	hsu1.read_buffer()
