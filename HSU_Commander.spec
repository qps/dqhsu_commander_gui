# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['HSU_Commander.py'],
             pathex=['C:\\Users\\gmarting\\PycharmProjects\\automated_hw_testing\\devicelib', 'C:\\Users\\gmarting\\PycharmProjects\\automated_hw_testing\\datanalysis'],
             binaries=[],
             datas=[
             ('C:/Windows/System32/Ni845x.dll', '.'),
             ('C:/Windows/System32/nivisa64.dll', '.'),
             ('C:/Windows/system32/NiViSv64.dll', '.'),
             ('C:/Windows/System32/NiSpyLog.dll', '.'),
             ('C:/Windows/System32/visa32.dll', '.'),
             ('C:/Windows/System32/visa64.dll', '.'),
             ('C:/Windows/System32/visaConfMgr.dll', '.'),
             ('C:/Windows/System32/visaUtilities.dll', '.'),
             ],
             hiddenimports=[
             'sklearn.utils._cython_blas',
             'sklearn.neighbors.typedefs',
             'sklearn.neighbors.quad_tree',
             'sklearn.tree._utils',
             'sklearn.utils._cython_blas',
             'pyvisa-py',
             'tkinter'
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='HSU_Commander',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='HSU_Commander')
