# DQHSU Commander

This project contains a port of the DQHSU commander written using python.
The GUI is written unsing the free wxPython framework. The project requires the
following libraries:

  * numpy
  * matplotlib
  * pandas
  * scipy
  * pyserial
  * wxpython
  * aenum
  
```
pip install numpy matplotlib pandas pyserial wxpython
```

Currenty it only supports one instance of a NI-8451 USB to SPI transceiver, so
it requires the computer to have the required drivers installed. It also requires 
the ni8451.py and pdvs2.py libraries available at the Automated HW Testing repository:

https://gitlab.cern.ch/qps/automated_hw_testing/-/tree/master/devicelib

Work is undergoing to support the FTDI UM232H usb to spi transceiver.


Development is being mygrated from Visual studio to pycharm community edition. 
